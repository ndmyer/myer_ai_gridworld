﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrassGrowth : MonoBehaviour
{
    
    private SpriteRenderer _grassSprite;

    public int grassCurrentHeight;
    public Color[] grassColorArray;
    public bool _grassEdible;
    public GameObject dirtTile;
    
    // Start is called before the first frame update
    void Start()
    {
        _grassSprite = transform.GetComponent<SpriteRenderer>();
        InvokeRepeating(nameof(GrassGrowing), 1.0f, 1.0f);
    }

    // Grow grassHeight over time to grassMaxHeight when at grassMaxHeight for witherTime set grassHeight to 0
    // with a chance of grassTile turning into dirt tile
    // Grass color changes with its height
    void GrassGrowing()
    {
        _grassSprite.color = grassColorArray[grassCurrentHeight];
        grassCurrentHeight += 1;

        if (grassCurrentHeight >= 3)
        {
            _grassEdible = transform;
        }

        if (grassCurrentHeight > 5)
        {
            grassCurrentHeight = 0;
            _grassEdible = false;
            
            var _randomIndex = Random.Range(0, 5);
            if (_randomIndex == 1)
            {
                Instantiate(dirtTile, transform.position, Quaternion.identity);
                Destroy(gameObject);
            }
        }
    }
    
    
}
