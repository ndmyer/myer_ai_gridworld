﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

public class SheepBrain : MonoBehaviour
{
    public int sheepCurrentHealth = 100;
    public GameObject _sheepChild;
    
    private bool _grassOnTile = false;
    private int _grassCurrentHieght = 0;

    public LayerMask _woldLayerMask;
    public LayerMask _grassLayerMask;
    public LayerMask _mateLayerMask;
    
    private Vector2 _wolfSearchSize = new Vector2(5, 5);
    private Vector2 _foodMateSearchSize = new Vector2(3, 3);
    private Vector2 _onGrassSearchSize = new Vector2(1, 1);
    private Collider2D[] _grassColliders;
    private Collider2D _grassBelowCollider;
    private Collider2D _grassSearchCollider;
    private Collider2D _wolfCollider;
    private Collider2D _sheepCollider;
    private float _minGrassDistance = 0;
    private int _randomIndex;
    

    private Vector2[] _wanderDirections = new[]
    {
        Vector2.down, Vector2.left, Vector2.up, Vector2.right, new Vector2(1, 1), 
        new Vector2(-1, -1), new Vector2(1, -1), new Vector2(-1, 1)
    };

    private Vector2 _chosenWanderDirection;
    private Vector3 _clampedMovement;


    private List<GameObject> _grassTiles; 
    
    // Calls SheepAILoop method every X seconds from start
    void Start()
    {
        InvokeRepeating(nameof(SheepAILoop), 1.0f, 1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Gizmos for the OverLap Boxes to help test
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, _wolfSearchSize);
        Gizmos.DrawWireCube(transform.position, _foodMateSearchSize);
        Gizmos.DrawWireCube(transform.position, _onGrassSearchSize);
    }

    //Sheeps main loop. Chooses sheeps action each check. 
    void SheepAILoop()
    {
        //Array.Clear(_grassColliders, 0, _grassColliders.Length);
        
        Mathf.Clamp(sheepCurrentHealth, 0, 100);
        
        // Box Colliders grabbing grass, wolf, and sheep positions 
        _grassColliders = Physics2D.OverlapBoxAll(transform.position, _foodMateSearchSize, 0, _grassLayerMask);
        _grassBelowCollider = Physics2D.OverlapBox(transform.position, _onGrassSearchSize, 0, _grassLayerMask);
        _wolfCollider = Physics2D.OverlapBox(transform.position, _wolfSearchSize, 0, _woldLayerMask);
        _sheepCollider = Physics2D.OverlapBox(transform.position, _foodMateSearchSize, 0, _mateLayerMask);
        sheepCurrentHealth -= 5;

        // Iterating over grass colliders to get closest grass
        foreach (var _grassCollider in _grassColliders)
        {
            float grassDistance = Vector3.Distance(_grassCollider.transform.position, transform.position);
            if (grassDistance < _minGrassDistance)
            {
                _grassSearchCollider = _grassCollider;
                _minGrassDistance = grassDistance;
            }
        }

        _minGrassDistance = 0;

        if (_grassBelowCollider != null)
        {
            _grassCurrentHieght = _grassBelowCollider.gameObject.GetComponent<GrassGrowth>().grassCurrentHeight;
        }

        // If sheep health is equal or below 0 die / destroy sheep
        if (sheepCurrentHealth <= 0)
        {
            Die();
        } // If wolf within sense range flee
        else if(_wolfCollider != null)
        {
            Flee();
        } // If health is over 75 & another Sheep within range mate
        else if (sheepCurrentHealth >= 75 && _sheepCollider != null)
        {
            Mate();
        } // If Health below 100 and there is grass on current tile eat
        else if (sheepCurrentHealth < 100 && _grassCurrentHieght >= 3)
        {
            Eat();
        } // If Health is below 100, there is no grass on current tile, and there is a/are grass tile(s) within range Seek to closest grass tile
        else if (sheepCurrentHealth < 100 && _grassSearchCollider != null)
        {
            Seek();
        } // If there are not sheep, grass, or wolves within range wander
        else
        {
            Wander();
        }
    }

    private void Die()
    {
        Destroy(this.gameObject);
    }

    // Picks a random tile within range and moves to it
    // Clamps sheep to stay within bound currently not working though
    private void Wander()
    {
        Debug.Log(name + " Wandered");
        
        _randomIndex = Random.Range(0, 8);
        transform.position += (Vector3)_wanderDirections[_randomIndex];
        _clampedMovement = transform.position;
        _clampedMovement.x = Mathf.Clamp(_clampedMovement.x, 0f, 9f);
        _clampedMovement.y = Mathf.Clamp(_clampedMovement.y, 0f, 9f);
        transform.position = _clampedMovement;
    }

    // Moves towards passed in transform
    private void Seek()
    {
        Debug.Log(name + " Is Seeking Grass");
        transform.position += transform.position - _grassSearchCollider.transform.position;
    }

    // Eats grass, removes grass, and recovers health
    private void Eat()
    {
        Debug.Log(name + " Is Eating Grass");
        sheepCurrentHealth += 25;
        _grassBelowCollider.gameObject.GetComponent<GrassGrowth>()._grassEdible = false;
        _grassBelowCollider.gameObject.GetComponent<GrassGrowth>().grassCurrentHeight = 0;
    }

    // Mates with closest sheep reducing self's and other sheeps health to half and instantiating a sheep at one fourth health
    private void Mate()
    {
        Debug.Log(name + " Is Mating");
        
        sheepCurrentHealth = sheepCurrentHealth / 2;
        _sheepCollider.gameObject.GetComponent<SheepBrain>().sheepCurrentHealth =
            _sheepCollider.gameObject.GetComponent<SheepBrain>().sheepCurrentHealth / 2;
        
        Instantiate(_sheepChild, transform.position, Quaternion.identity);

    }

    // Moves away from passed in position
    private void Flee()
    {
        Debug.Log(name + " Is Fleeing Wolf");
        
        transform.position -= transform.position - _wolfCollider.transform.position;
    }
}
