﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public int[,] Grid;
    public int rows;
    public int columns;

    public GameObject[] tiles;
    private GameObject _currentTile;

    // For every row in rows and column in columns spawn a tile on the grid
    void Start()
    {
        Grid = new int[ columns, rows];
       
        for (int _column = 0; _column < columns; _column++)
        {
            for (int _row = 0; _row < rows; _row++)
            {
                SpawnTile(_column, _row, Grid[_column, _row]);
            }
        }
    }

    // Method that chooses what tile to spawn from an array, instantiates them, and positions them on the grid
    private void SpawnTile(int x, int y, int i)
    {
        _currentTile = tiles[Random.Range(0, tiles.Length)];
        Instantiate(_currentTile, new Vector2(x, y), Quaternion.identity);
    }

    
    void Update()
    {
        
    }
}
