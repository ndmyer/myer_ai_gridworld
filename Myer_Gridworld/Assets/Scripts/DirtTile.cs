﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtTile : MonoBehaviour
{
    public GameObject grassTile;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating(nameof(DirtChange), 1.0f, 1.0f);
    }

    void DirtChange()
    {
        var _randomIndex = Random.Range(0, 20);
        if (_randomIndex == 1)
        {
            Instantiate(grassTile, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
    // Chance of dirtTile turning into grassTile
    void Update()
    {
        
    }
}
